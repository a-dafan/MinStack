stack<int> st;

class MinStack {
public:
	MinStack() {
		_a = nullptr;
		_size = _capacity = 0;
	}

	~MinStack()
	{
		delete[] _a;
		_a = nullptr;
		_size = _capacity = 0;
		while (!st.empty())
		{
			st.pop();
		}
	}

	void reserve(int n)
	{
		if (n > _capacity)
		{
			int* tmp = new int[n];
			for (int i = 0; i < _size; i++)
			{
				tmp[i] = _a[i];
			}
			delete[] _a;
			_a = tmp;
			_capacity = n;
		}
	}

	bool empty()
	{
		return _size == 0;
	}

	void push(int val) {
		if (_size == _capacity)
		{
			int newCapacity = _capacity ? 2 * _capacity : 5;
			reserve(newCapacity);
		}
		if (empty() || val < _a[st.top()])
			st.push(_size);
		_a[_size++] = val;
	}

	void pop() {
		assert(!empty());
		if (_size - 1 == st.top())//如果删除的是最小值的下标 st也同步删除
			st.pop();
		_size--;
	}

	int top() {
		assert(!empty());
		return _a[_size - 1];
	}

	int getMin() {
		assert(!empty());
		return _a[st.top()];
	}
private:
	int* _a;
	int _size;
	int _capacity;
};